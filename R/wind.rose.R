####################################################################################################
#               wind.rose
####################################################################################################
#   purpose:    plot wind rose
#   args:       freq    (2D-matrix) frequency distribution of wind direction and speed
#               fnum    Number of reference circumferences to plot.
#               fint    Frequency steps (in %) between reference circumferences.
#               flab    Parameter indicating which circumferences must be labelled:
#                       0: Label outer circumference only,
#                       1: Label all circumferences,
#                       >1: Label only each flab'th circumference
#                       <0: Do not label any circumference.
#               ang     Angle along which circumferences will be labelled.
#               col     Colors to fill the frequency polygons.
#               mar  Margins vector for the plot (to be passed to 'par').
#               key     Set to FALSE if you do not want a legend of the wind-rose,
#                       that will otherwise be plotted if frequencies are supplied by
#                       speed intervals.
#               uni     Speed units for the legend header.
#				direction if TRUE plot direction markers on wind rose
#               ...     Other graphic parameters passed to plot
#   returns:    invisible()
#   author:     taken from package climatol modified by stephan.henne@empa.ch
#   version:    0.1-051124
#   requires:   -
####################################################################################################
wind.rose <- function (freq, fnum = 4, fint, flab = 1, ang = 3 * pi/16, 
		col = rainbow(nrow(freq),0.5, 0.92, start = 0.3, end = 0.8), mar = c(0, 0, 1, 0),
		key = TRUE, key.title = "WS (m/s)", c.title=NULL, direction=TRUE, ws.rng=NULL, ...){

	if (is.data.frame(freq)){
		ws.mean = NULL
	} else {
		ws.mean = freq$ws.mean
		freq = freq$freq
	}

	tfreq = apply(freq, 2, sum)
	maxf = max(tfreq)/sum(tfreq) * 100
	if (missing(fint)) {	
		if (is.na(maxf))
			maxf = 10
		if (maxf >= 40)
			fint = 10
		else fint = 5
	}
	if (missing(fnum))
		fnum = trunc(maxf/fint) + 1
	if (is.vector(freq))
		nr <- 1
	else nr <- nrow(freq)
	ndir <- length(freq)
	fmax <- fnum * fint
	tot <- sum(freq)
	fr <- 100 * freq/tot
	key <- (nr > 1) && key
	if (key)
		mlf <- 2
	else mlf <- 1
	par(mar = mar)
	fx <- cos(pi/2 - (2 * pi/ndir * 0:(ndir - 1)))
	fy <- sin(pi/2 - (2 * pi/ndir * 0:(ndir - 1)))
	plot(fx, fy, xlim = c(-fmax - mlf * fint, fmax + fint), 
		ylim = c(-fmax-fint, fmax + fint), xaxt = "n", yaxt = "n", xaxs = "i",
		yaxs = "i", xlab = "", ylab = "", bty = "n", asp = 1, type = "n", ...)

	if (nr == 1) {
		cx <- fx * fr
		cy <- fy * fr
	} else {
		f <- apply(fr, 2, sum)
		cx <- fx * f
		cy <- fy * f
		for (i in nr:2) {
			f <- f - fr[i, ]
			cx <- c(cx, NA, fx * f)
			cy <- c(cy, NA, fy * f)
		}
	}
	polygon(cx, cy, col = col[nr:1])
	symbols(c(0 * 1:fnum), c(0 * 1:fnum), circles = c(fint * 1:fnum),
		inches = FALSE, add = TRUE,  lwd=.6)
	segments(0 * 1:ndir, 0 * 1:ndir, fmax * fx, fmax * fy, lwd=.6)

	if (!is.null(ws.mean)){
		if (is.null(c.title)){
			c.title = ""
		}
		if (is.null(ws.rng)){
			ws.rng= range(pretty(range(c(ws.mean$mean, ws.mean$median), na.rm=TRUE), fnum))
		}
		ws.diff = diff(ws.rng)	

		#	means
		wx = (ws.mean$mean-ws.rng[1])/ws.diff*fmax*fx
		wy = (ws.mean$mean-ws.rng[1])/ws.diff*fmax*fy
		wx[length(wx)+1] = wx[1]
		wy[length(wy)+1] = wy[1]
		lines(wx, wy, lwd=par("lwd")*2, col=2)

		#	medians 
		wx = (ws.mean$median-ws.rng[1])/ws.diff*fmax*fx
		wy = (ws.mean$median-ws.rng[1])/ws.diff*fmax*fy
		wx[length(wx)+1] = wx[1]
		wy[length(wy)+1] = wy[1]
		lines(wx, wy, lwd=par("lwd")*2, col=2, lty=2)
	}
																								  	
	if(direction){
		fmaxi <- fmax + fint/4
		text(0, 1.02 * fmaxi, "N", font = 2)
		text(0, -1.02 * fmaxi, "S", font = 2)
		text(1.02 * fmaxi, 0, "E", font = 2)
		text(-1.02 * fmaxi, 0, "W", font = 2)
	}

	if (flab >= 1){
		for (i in 1:fnum) {
			if (i%%flab == 0){
				text(i * fint * cos(ang), i * fint * sin(ang), paste(i * fint, "%"))
			}
		}
		if(!is.null(ws.mean)){
			for (i in 1:fnum) {
				if (i%%flab == 0){
					text(i * fint * cos(-ang), i * fint * sin(-ang), 
						paste(signif((ws.rng[1] + i*fint/fmax*ws.diff),2)), col=2)
				}
			}
			text((i+1) * fint * cos(-ang), (i+1) * fint * sin(-ang), c.title, col=2)
		}
	} else if (flab == 0) {
		text(fmax * cos(ang), fmax * sin(ang), paste(fmax, "%"))
		if(!is.null(ws.mean)){
			text(fmax * cos(-ang), fmax * sin(-ang), paste(signif(ws.max,2), c.title, sep="\n"))
		}
	}

	if (key) {
		legend("topleft", inset = 0.02, pch = 22, pt.bg = col,
			col = par("fg"), pt.cex = 1.5, legend = attr(freq,"row.names"), 
			bg = par("bg"), title = key.title)
	}

	invisible()
}


###   EXAMPLE with normal distributed wind components
#require(meteoconv)
#nn = 1000
#dat = data.frame(UU=rnorm(nn), VV=rnorm(nn))
#tmp = UU_VV_2_WS_WD(dat$UU, dat$VV)
#dat$WS = tmp$WS
#dat$WD = tmp$WD
#res = wind.freq(dat, key.title = "WS (m/s)", plot=FALSE)
#wind.rose(res, fint=1)
##
##res = wind.freq(dat, ws.para="cc", unit = "CO (ppb)")
