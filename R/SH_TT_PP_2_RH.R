#	sh 	kg/kg water vapor mass/(total air mass) 
#	pp  in hPa
#	tt  in K or degree C
SH_TT_PP_2_RH = function(sh, tt, pp){
	epsilon = 0.621979	

	e = pp * sh /((1-epsilon)*sh + epsilon)
	es = ES(tt)

	rh = e/es * 100

	return(rh)
}
